import tkinter as tk
import graphviz
from tkinter import ttk
from data_structure.people import Person
from core.hash_table import HashTable


def render_main_window():
    window = tk.Tk()
    window.geometry('450x300')
    window.title("HashTable Inspector")
    label = tk.Label(
        window,
        text="Welcome to HashTable inspector",
        wraplength=350
    )
    label.grid(column=0, row=0, columnspan=2, padx=5, pady=2)
    return window


def render_display_file_contents_button(window: tk.Tk, event_handler):
    read_file_btn = tk.Button(window, text='Read file contents', command=event_handler, width=15)
    read_file_btn.grid(column=0, row=1, sticky='W', padx=5, pady=2)


def render_display_file_contents_list(list_of_people: list[Person]):
    tree_window = tk.Tk()
    tree_window.title('Display file contents')
    tree_window.geometry('800x600')
    tree_view = ttk.Treeview(tree_window, height=50)
    tree_view['columns'] = ('name', 'email', 'profession')

    tree_view.heading('#0', text='#')
    tree_view.heading('name', text='Name')
    tree_view.heading('email', text='Email')
    tree_view.heading('profession', text='Profession')

    index = 0
    for person in list_of_people:
        tree_view.insert('', 'end', text=str(index),
                         values=(person.name, person.email, person.profession))
        index += 1

    tree_view.pack(fill='both', expand=True)


def render_display_hash_table_button(window: tk.Tk, event_handler, label="Display hash table", row=1, column=1):
    read_file_btn = tk.Button(window, text=label, command=event_handler, width=15)
    read_file_btn.grid(column=column, row=row, sticky='W', padx=5, pady=2)


def render_display_hash_table(hash_table: HashTable):
    dot = graphviz.Digraph(comment='The Round Table')

    index = 0
    node_count = 0
    max_node_count = 50
    for hash_table_record in hash_table.table:
        if node_count > max_node_count:
            break

        dot.node(str(index), str(index), style='filled', fillcolor='black', fontcolor='white')

        if len(hash_table_record) != 0:
            prev_name: str = ''
            for person_record in hash_table_record:
                person = person_record[1][0]
                dot.node(person.name, person.name, shape='box')

                if prev_name == '':
                    dot.edge(str(index), person.name, constraint='true')
                else:
                    dot.edge(prev_name, person.name, constraint='true')
                prev_name = person.name
        if index > 0:
            dot.edge(str(index - 1), str(index), constraint='true')
                
        index += 1
        node_count += 1
    dot.render("graph.gv")


def render_add_button(window: tk.Tk, hash_table: HashTable):
    add_record_btn = tk.Button(window, text='Add record',
                               command=lambda: render_insertion_form(hash_table), width=15)
    add_record_btn.grid(row=2, column=1, sticky='W', padx=5, pady=2)


def render_insertion_form(hash_table: HashTable):
    add_record_window = tk.Tk()
    add_record_window.geometry('400x300')
    add_record_window.title("Manually adding record")

    name_label = tk.Label(add_record_window, text='Name')
    name_label.pack()
    name_field = tk.Entry(add_record_window)
    name_field.pack()

    email_label = tk.Label(add_record_window, text='Email')
    email_label.pack()
    email_field = tk.Entry(add_record_window)
    email_field.pack()

    profession_label = tk.Label(add_record_window, text='Profession')
    profession_label.pack()
    profession_field = tk.Entry(add_record_window)
    profession_field.pack()

    add_btn = tk.Button(
        add_record_window,
        text='Add record',
        command=lambda: hash_table.insert(name_field.get(),
                                          Person(name_field.get(), email_field.get(), profession_field.get()))
    )
    add_btn.pack()


def render_input_field(window: tk.Tk):
    search_field = tk.Entry(window, width=35)
    search_field.grid(row=3, column=0, columnspan=2, sticky='W', padx=2, pady=2)
    search_field.insert(0, 'Johna Alice')
    return search_field


def render_search_button(window: tk.Tk, get_val_fn, event_handler):
    search_test_btn = tk.Button(window, text='Search test',
                                command=lambda: event_handler(get_val_fn()), width=15)
    search_test_btn.grid(row=4, column=0, sticky='W')


def render_remove_button(window: tk.Tk, hash_table: HashTable, get_value_fn):
    search_test_btn = tk.Button(window, text='Removal test',
                                command=lambda: hash_table.delete(get_value_fn()), width=15)
    search_test_btn.grid(row=4, column=1, sticky='W')
#
#
# def render_remove_button(window: tk.Tk, get_value_fn, event_handler):
#     search_test_btn = tk.Button(window, text='Remove',
#                                 command=lambda: event_handler(get_value_fn()), width=15)
#     search_test_btn.grid(row=5, column=0, sticky='W')
#
# def render_display_tree_button(window: tk.Tk, tree: BTree):
#     display_tree_btn = tk.Button(window, text='Display tree', command=lambda: render_display_tree(tree), width=15)
#     display_tree_btn.grid(row=1, column=1, sticky='W', padx=5, pady=2)
