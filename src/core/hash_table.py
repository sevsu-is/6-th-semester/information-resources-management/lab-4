class HashTable:
    def __init__(self, capacity=100):
        self.capacity = capacity
        self.table = [[] for _ in range(capacity)]

    def hash(self, key):
        return hash(key) % self.capacity

    def insert(self, key, value):
        index = self.hash(key)
        for item in self.table[index]:
            if item[0] == key:
                item[1].append(value)
                return
        self.table[index].append((key, [value]))

    def delete(self, key):
        index = self.hash(key)
        for i, item in enumerate(self.table[index]):
            if item[0] == key:
                del self.table[index][i]
                return

    def search(self, key):
        index = self.hash(key)
        for item in self.table[index]:
            if item[0] == key:
                return item[1]
        return None
