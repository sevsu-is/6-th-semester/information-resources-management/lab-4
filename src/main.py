import graphviz
from core.hash_table import HashTable
from utility.csv_parser import read_people_csv
from ui import (
    render_main_window,
    render_display_file_contents_button,
    render_display_file_contents_list,
    render_display_hash_table_button,
    render_display_hash_table,
    render_add_button,
    render_input_field,
    render_search_button,
    render_remove_button
)


def main():
    filename = 'people.csv'
    hash_table = HashTable(15)

    window = render_main_window()
    render_display_file_contents_button(
        window,
        lambda: render_display_file_contents_list(read_people_csv(filename))
    )

    render_display_hash_table_button(
        window,
        lambda: render_display_hash_table(hash_table)
    )

    render_display_hash_table_button(
        window,
        lambda: render_display_hash_table(import_people_from_csv(filename)),
        label='Build table (from file)',
        row=2,
        column=0,
    )

    render_add_button(window, hash_table)
    name_input_field = render_input_field(window)

    render_search_button(window, lambda: name_input_field.get(), lambda: None)
    render_remove_button(window, hash_table, name_input_field.get)

    window.mainloop()


def import_people_from_csv(filename: str) -> HashTable:
    list_of_people = read_people_csv(filename)
    hash_table_capacity = len(list_of_people) + 100
    hash_table = HashTable(hash_table_capacity)

    for person in list_of_people:
        hash_table.insert(person.name, person)

    return hash_table


if __name__ == '__main__':
    main()
