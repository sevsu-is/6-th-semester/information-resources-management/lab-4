import csv
from data_structure.people import Person


def _read_csv_into_lines(filename: str) -> list[list[str]]:
    lines: list[list[str]] = []

    with open(filename, 'r') as file:
        csv_reader = csv.reader(file)
        next(csv_reader)

        for i, row in enumerate(csv_reader):
            lines.append(row)

    return lines


def read_people_csv(filename: str) -> list[Person]:
    lines = _read_csv_into_lines(filename)
    list_of_people: list[Person] = []

    for line in lines:
        name, email, profession = line
        list_of_people.append(Person(name, email, profession))

    return list_of_people
